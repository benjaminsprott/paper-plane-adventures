﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour
{

  public float maxSpeed;
  public float acceleration;
  public float deceleration;
  public float rotationSpeed;

  private float currentSpeed = 0;
  private Rigidbody myRgidbody;

  // Use this for initialization
  void Start()
  {
    myRgidbody = GetComponent<Rigidbody>();
  }

  // Update is called once per frame
  void Update()
  {
    UpdateMovement();
  }

  private void UpdateMovement()
  {
    float hInput = Input.GetAxis("Horizontal");
    float vInput = Input.GetAxis("Vertical");

    Vector3 inputV = new Vector3 (hInput, 0, vInput).normalized;
    if (hInput != 0f || vInput != 0f)
    {
      // accelerate
      currentSpeed += acceleration * Time.deltaTime;
      if (currentSpeed > maxSpeed)
        currentSpeed = maxSpeed;

      // rotate towards input angle of hInput and vInput
      //Quaternion lookRotation = Quaternion.LookRotation(new Vector3(hInput, 0f, vInput));
      //transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime);
    }
    else
    {
      // decelerate
      currentSpeed -= 3 * acceleration * Time.deltaTime;
      if (currentSpeed < 0f)
        currentSpeed = 0f;
    }
    // fly forward
    myRgidbody.velocity = currentSpeed * inputV;

  }
}
